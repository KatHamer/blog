from flask import render_template, Response, abort
from app import app
from pathlib import Path
import time
import jinja2

@app.errorhandler(404)
def page_not_found(error):
   return render_template('404.html', title = '404'), 404

@app.route('/')
def index():
    """Render the home page"""
    return render_template('home.html')

@app.route('/test')
def example():
    """Render the home page"""
    return render_template('test.html')

@app.route('/post/<post_slug>')
def get_post(post_slug):
    try:
        return render_template(f'post_{post_slug}.html')
    except jinja2.exceptions.TemplateNotFound:
        abort(404)