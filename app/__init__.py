from flask import Flask
import logging
from logging import handlers

LOG_BASE_FILENAME = "blog.log"

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.StreamHandler(),
        handlers.RotatingFileHandler(
            # 250 KB
            LOG_BASE_FILENAME,
            maxBytes=(25 * 10000),
            backupCount=7,
        ),
    ],
)

app = Flask(__name__)


from app import routes